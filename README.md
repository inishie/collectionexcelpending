
**collectionExcelPending**

	指定した文字列をExcelで検索します
	(Excelの未完了情報を抽出)

---

[TOC]

---

## Usage

	特定の文字列を検索したいExcelファイルを引数に指定して起動します。
	ドラッグ＆ドロップ操作用にバッチファイルを追加しています。
	ex) cscript //nologo collectionExcelPending.vbs C:/Test.xlsx

	検索したい内容は iniファイル で指定できます。
	findconfig.ini
		findMode : 検索形式 (0:前方の文字を問わず. 1:前方が英字以外, 2:前後が英字以外)
		stringFile : ｢検索文字リスト｣のファイルパス。検索したい文字列を、改行区切りで列挙。

	バッチファイルを使用した場合、検索結果の内容はログファイルに出力します。
	ファイル名規則:	list-YYYYMMDD.log
	区切り文字:	タブ(Tab)
	出力項目:
		ファイルのフルパス
		ファイル名(拡張子を含む)
		シート名
		セル位置
		セルの文字列 (一致する文字列が無い場合、｢該当なし｣の行を出力)

	ex)
		C:/Test1.xlsx	Test1.xlsx	Sheet1	[2:3]	TODO

## Note

	・bat, vbs, ini, sjis は日本語を含むため、ファイルのエンコードはsjisを前提としています。
	・実行端末に Microsoft Office Excel が必要です。
	・｢検索文字リスト｣の作成例は stringList.sjis です。
	　テキストファイルあれば、拡張子は自由です。拡張子(sjis)は、バージョン管理の都合です。
	・文字列の検索方法の都合上、1つのセルにn件の検索文字が該当した場合、
	　ログファイルに、同じセルの情報が n行出力されます。


---

## References

	REONTOSANTA
	VBScriptでExcelを操作する - REONTOSANTA
	http://knowledge.reontosanta.com/archives/838

	WSH@Workshop
	コマンドラインからパラメータを受け取る - WSH@Workshop
	http://wsh.style-mods.net/topic6.htm

	Odyssey Communications Inc.
	条件に当てはまるセルを検索する(Find/FindNext/FindPreviousメソッド) ：Excel VBA｜即効テクニック｜Excel VBAを学ぶならmoug
	https://www.moug.net/tech/exvba/0050116.html

	ITガイド
	VBScript: ファイルを読み込み、配列に格納する
	https://step-learn.com/article/vbscript/043-file-read-array.html


## Author

	inishie
	inishie77@hotmail.com
	https://bitbucket.org/inishie/

## License

Copyright (c) 2022 inishie.  
Released under the [MIT license](https://en.wikipedia.org/wiki/MIT_License).
