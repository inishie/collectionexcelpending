' Excelの未完了情報を抽出
' ex) cscript //nologo collectionExcelPending.vbs C:/Test.xlsx
' 出力
' ex) C:/Test1.xlsx	Test1.xlsx	Sheet1	[1:1]	TODO
' ex) C:/Test2.xlsx	Test2.xlsx			該当なし


Option Explicit

Dim fso, oXlsApp, xlBook, oSheet, objText
Dim strPara1, strHedder, basename, extension, fileName, strOutput, sIdx, rootPath, strText, arrText, wIdx
Dim findMode, stringFile  'ini指定

If WScript.Arguments.Count <> 1 Then
	WScript.Echo "パラメータ不備"
	WScript.Quit 1
End If

Set fso = CreateObject("Scripting.FileSystemObject")
If fso Is Nothing Then
	' FileSystemObject起動失敗
	WScript.Echo "FileSystemObject起動失敗"
	WScript.Quit 1
End If

rootPath = fso.getParentFolderName(WScript.ScriptFullName) & "\"

' 設定値読込
ExecuteGlobal fso.OpenTextFile(rootPath & "findconfig.ini").ReadAll()

If (findMode = "" Or stringFile = "") Then
	'環境設定は必須
	WScript.Echo "環境設定してください"
	WScript.Quit 1
End If
If (False = fso.FileExists(stringFile)) Then
	'検索文字リストは必須
	WScript.Echo "検索文字リストのファイルがありません"
	WScript.Quit 1
End If

Set objText = fso.OpenTextFile(stringFile)
strText = objText.ReadAll
objText.Close

arrText = Split(strText, vbCrLf)  ' 改行で分割


' 引数(フルパス/相対パス)
strPara1 = WScript.Arguments(0)
strHedder = strPara1

' ファイル名
basename = fso.GetBaseName(strPara1)
extension = fso.GetExtensionName(strPara1)
fileName = basename
If Len(extension) <> 0 Then
	fileName = fileName & "." & extension
End If

strHedder = strHedder & vbTab & fileName

' Excel起動
Set oXlsApp = CreateObject("Excel.Application")
If oXlsApp Is Nothing Then
	' Excel起動失敗
	WScript.Echo "Excel起動失敗"
	Set fso = Nothing
	WScript.Quit 1
End If

' Excel起動成功

oXlsApp.Application.Visible = False		' --Excel表示（falseにすると非表示にできる）
oXlsApp.Application.DisplayAlerts = False	' --Excelの警告を非表示にする
' WScript.Sleep(1000)				' --1秒待つ

strOutput = ""		' 出力内容(n行)

On Error Resume Next ' 強行
Set xlBook = oXlsApp.Application.Workbooks.Open(strPara1,,True,,,,,,,,,,,,1)		' --ブックを開く(読み取り専用,回復あり)
If Err.Number <> 0 Then
	' ファイルに問題あり
	strOutput = strOutput & strHedder & vbTab & vbTab & vbTab & "検索失敗"
	WScript.Echo strOutput
	Set fso = Nothing
	WScript.Quit 1
End If
On Error Goto 0 ' 解除


For sIdx = 1 To xlBook.Worksheets.Count
	Set oSheet = oXlsApp.Worksheets(sIdx)		' --シート別

	' 検索
	For wIdx = 0 To UBound(arrText)
		strOutput = strOutput & findPendString(oSheet, strHedder, arrText(wIdx), findMode)
	Next

Next

If strOutput = "" Then
	' ファイルに該当なし
	strOutput = strOutput & strHedder & vbTab & vbTab & vbTab & "該当なし"
End If

WScript.Echo strOutput

' --Excel終了
xlBook.Close False
oXlsApp.Quit
' --Excelオブジェクトクリア
Set oXlsApp = Nothing

Set fso = Nothing

WScript.Quit 0


'**********************************
'用途: 検索処理
'引数: oSheet     Worksheet
'      strHedder  検出時の基本情報(改行含まず)
'      keyword    検索文字
'      isWord     判定方法 (0:前方の文字を問わず. 1:前方が英字以外, 2:前後が英字以外)
'戻値: 出力文字 (該当情報)
'**********************************
Function findPendString(oSheet, strHedder, keyword, isWord)
	findPendString = ""
	Dim oRange
	Dim firstAddress, cellVal

	If keyword = "" Then
		Exit Function
	End If

	' セル選択
	Set oRange = oSheet.Cells.Find(keyword)
	If Not oRange Is Nothing Then
		'該当あり(n件)
		firstAddress = oRange.Address

		Do
			' 行: oRange.Row
			' 列：oRange.Column
			cellVal = oSheet.Cells(oRange.Row, oRange.Column).value

			If ((isWord = 0) Or (verificationWord(cellVal, keyword) >= isWord)) Then
				' 指定文字が確定 (単語が前方一致 など)
				If findPendString <> "" Or strOutput <> "" Then
					' 改行が必要 (内部文字 か 結果文字 に出力値がある)
					findPendString = findPendString & vbCrLf
				End If
				cellVal = Replace(cellVal, vbCrLf, " ") ' 改行の抑止(補強)
				cellVal = Replace(cellVal, vbCr, " ") ' 改行の抑止(補強)
				cellVal = Replace(cellVal, vbLf, " ") ' セル内改行の抑止
				cellVal = Replace(cellVal, vbTab, " ") ' タブの抑止
				findPendString = findPendString & strHedder & vbTab & oSheet.Name & vbTab & "[" & oRange.Row & ":" & oRange.Column & "]" & vbTab & cellVal
			End If

			Set oRange = oSheet.Cells.FindNext(oRange)
			If oRange Is Nothing Then
				' 終了
				Exit Do
			End If
		Loop Until firstAddress = oRange.Address		' 先頭に戻るまで続ける
	End If

End Function

'**********************************
'用途: 単語の検証 (大文字小文字の差を許容)
'引数: fullText   文字の全文
'      keyword    検索文字
'戻値: 判定結果   0:別の単語, 1:単語が前方一致, 2:単語が一致
'**********************************
Function verificationWord(fullText, keyword)
	verificationWord = 2
	Dim objRE
	Dim preIdx, preWord, sufIdx, regResult, sufWord

	Set objRE = CreateObject("VBScript.RegExp")
	objRE.Pattern = "[a-zA-Z]"

	preIdx = InStr(1, fullText, keyword, vbTextCompare) - 1
	preWord = ""
	If preIdx >= 1 Then
		' 前方に文字がある
		preWord = Mid(fullText, preIdx, 1)
	End If
	If Len(preWord) > 0 Then

		regResult = objRE.Test(preWord)

		If regResult Then
			' 前方に英字があるため、別の単語とみなす
			verificationWord = 0
		End If
	End If

	If verificationWord <> 0 Then
		sufIdx =  InStr(1, fullText, keyword, vbTextCompare) + Len(keyword)
		sufWord = ""
		If Len(fullText) >= sufIdx Then
			sufWord = Mid(fullText, sufIdx, 1)
		End If
		If Len(sufWord) > 0 Then

			regResult = objRE.Test(sufWord)

			If regResult Then
				' 後方に英字があるため、前方一致の単語とみなす
				verificationWord = 1
			End If
		End If
	End If

	Set objRE = Nothing

End Function