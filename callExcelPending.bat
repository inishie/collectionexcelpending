@ECHO OFF
cd /d  %~dp0

if "%~1"=="" (
	echo Please specify a excel file.
	timeout 4
	exit /B 1
)

set YYYYMMDD=%DATE:/=%
set LOG_FILE=list-%YYYYMMDD%.log

cscript //nologo collectionExcelPending.vbs "%~1" >> %LOG_FILE%

type %LOG_FILE%

timeout 4
exit /B 0
